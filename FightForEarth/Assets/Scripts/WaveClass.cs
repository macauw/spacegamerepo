﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveClass : MonoBehaviour
{
	public enum FORMATION { V_FORMATION, H_LINE, V_LINE, ECHELON_LEFT, ECHELONG_RIGHT, FOUR_FINGER, DUAL_ELEMENT, BOX, WEDGE, DIAMOND, NO_FORMATION};
	[System.Serializable]
	public struct Wave
	{
		public GameObject spawnPoint;																					// Point where the object will spawn
		public GameObject destination;																					// Destination of spawned object
		public GameObject objectToSpawn;																				// Object to spawn
		public FORMATION FORMATION;																						// The formation of the object
		public float offSett;																							// Universal offset this should be set dependant on size of spawned object
		public int amount;																								// Amount that will be spawned
		public bool defeated;																							// Checksum if the wave is defeated early
		public int timeOutTime;																							// The time before the objects will disengage off screen
		public int pauseTime;																							// The time before another wave can spawn
	}
}
