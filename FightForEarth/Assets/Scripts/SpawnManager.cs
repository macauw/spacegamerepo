﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour 
{
	public WaveClass.Wave[] myWaves;
	private GameObject myObject;
	public waypointMovement myWaypointmovement;
	private Vector3[] offsetVector3;
	public float offset;
	public float startupTime;
	public float time;
	private bool start;
	private bool done;
	public int waveCounter;
	public int spawnCounter;
	private int locationCounter;

	void Start()
	{
		time = Time.time;
		start = false;
		done = false;
	}

	// Update is called once per frame
	void Update () {
		if (time + startupTime < Time.time)
			start = true;
		if (start == true && done == false)
		{
			for (waveCounter = 0; waveCounter < myWaves.Length; waveCounter++)
			{
				float universaloffset = myWaves[waveCounter].offSett;
				offsetVector3 = new Vector3[myWaves[waveCounter].amount];
				locationCounter = 0;
				switch (myWaves[waveCounter].FORMATION)																	// Case machine for calculating formations
				{
					case WaveClass.FORMATION.V_FORMATION:
						
						for (spawnCounter = 1; spawnCounter < myWaves[waveCounter].amount; spawnCounter++)
						{
							if (spawnCounter % 2 == 0)
							{
								offsetVector3[spawnCounter].x = offsetVector3[spawnCounter - 1].x * -1f;
								offsetVector3[spawnCounter].y = offsetVector3[spawnCounter - 1].y;
							}
							else
							{
								locationCounter++;
								offsetVector3[spawnCounter].x = locationCounter * universaloffset;
								offsetVector3[spawnCounter].y = locationCounter * universaloffset * 0.5f;
							}
						}
						break;
				}
				for (spawnCounter = 0; spawnCounter < myWaves[waveCounter].amount; spawnCounter++)
				{
					myObject = Instantiate(myWaves[waveCounter].objectToSpawn,                                              // instantiate object at spawnpoint + offset
					                       myWaves[waveCounter].spawnPoint.transform.position + offsetVector3[spawnCounter],                         // myObject will be used to change the destination with the offset
										   myWaves[waveCounter].spawnPoint.transform.rotation);                                        // this allows the enemies to fly into the screen in formation
					myWaypointmovement = myObject.GetComponent<waypointMovement>();
					if (myWaypointmovement != null)
					{
						myWaypointmovement.DestinationV3 = myWaves[waveCounter].destination.transform.position + offsetVector3[spawnCounter];
						myWaypointmovement.bStartTravel = true;
					}
					// line to set destination based on destination point + offset TODO
				}
			}
			done = true;
		}
	}
}
