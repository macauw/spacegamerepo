﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waypointMovement : MonoBehaviour {

	public float fSpeed;
	public float fMaxSpeed;
	public float fAcceleration;
	public float fDeceleration;
	public Vector3 DestinationV3;
	public Rigidbody2D myRB2D;
	public bool bStartTravel;
	public bool bDestionationReached;

	// Use this for initialization
	void Start () {
		myRB2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if (bStartTravel == true)
		{
			bDestionationReached = false;
			if (fSpeed < fMaxSpeed)
			{
				fSpeed += fAcceleration * Time.deltaTime;
			}
		}
	}

	void FixedUpdate()
	{
		myRB2D.MovePosition(Vector3.MoveTowards(transform.position, DestinationV3, fSpeed));
	}
}
