﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingScript : MonoBehaviour {

	public GameObject pooledObj;
	public int pooledAmount;
	public bool willGrow = true;

	List<GameObject> pooledObjects;

	// Use this for initialization
	void Start () {
		pooledObj = GetComponent<Weapon>().pfProjectile;
		pooledObjects = new List<GameObject>();
		for (int i = 0; i < pooledAmount; i++)
		{
			// Instantiate object that has to be pooled set it to inactive and add it to the list
			GameObject myobj = Instantiate(pooledObj, Camera.main.transform);
			myobj.SetActive(false);
			pooledObjects.Add(myobj);
		}
	}

	public GameObject GetPooledObject()
	{
		for (int i = 0; i < pooledObjects.Count; i++)
		{
			// check if object is not active in the scene then return that object
			if (!pooledObjects[i].activeInHierarchy)
			{
				return pooledObjects[i].gameObject;
			}
		}
		// if there is not an object that is inactive add a new object
		if (willGrow)
		{
			GameObject obj = Instantiate(pooledObj);
			pooledObjects.Add(obj);
			return obj;
		}
		return null;
	}
}
