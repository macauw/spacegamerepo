﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public int iDamage;
	private string strTarget;
	public float fMaxSpeed;
	public float fAccel;
	public float fDeathtimer;
	public bool shotByPlayer;
	private Rigidbody2D rbProjectile;
	public GameObject pfImpact;
	private RaycastHit2D ray_Results;
	public Vector3 v3Speed;				// vector for speed
	public Vector2 v2Direction;			// used for hit detection

	void Start()
	{
		if (shotByPlayer == true)
		{
			strTarget = "Enemy";   		// layer for hitting enemy
			v2Direction = Vector2.up;	// shoot up when player shoots
		}
		else 
		{
			strTarget = "Player";   	// layer for hitting player
			v2Direction = Vector2.down; // shoot down when enemy shoots
			fAccel = fAccel * -1; 		// Reverse acceleration
			fMaxSpeed = fMaxSpeed * -1;	// Reverse the max allowed speed
		}
		rbProjectile = GetComponent<Rigidbody2D>();
	}

	void Update () 
	{
		v3Speed.y =+ fAccel * Time.deltaTime;
		if (v3Speed.y > fMaxSpeed || v3Speed.y < fMaxSpeed)
		{
			v3Speed.y = fMaxSpeed;
		}
	}

	void FixedUpdate()
	{
		int iLayerMask = (LayerMask.GetMask(strTarget));// Layer that the projectile will hit
		//Debug.Log(LayerMask.GetMask(target));
		ray_Results = Physics2D.Raycast(rbProjectile.position, v2Direction, v3Speed.y, iLayerMask);	// Shoot raycast to determine if an enemy is in the bullets path
		if (ray_Results)	// If raycast hits something with the right layer then
		{
			Vector3 tVector3 = Vector3.zero;															// Store distance in a temporary vector
			tVector3.y = ray_Results.distance;
			rbProjectile.transform.Translate(tVector3);                                // Move projectile to target collider will make it seem it hit the target
			HitReg(ray_Results);
		}
		else 
		{
			rbProjectile.transform.Translate(v3Speed);									// Move regularly when no target hit by raycast
		}
	}

	void HitReg(RaycastHit2D hitInfo)
	{
		if (shotByPlayer == true)
		{
			Hitcalc enemy = hitInfo.transform.gameObject.GetComponent<Hitcalc>();
			if (enemy != null & hitInfo.transform.gameObject.CompareTag("Enemy"))
			{
				Debug.Log("Enemy taking damage");
				enemy.takeDamage(iDamage);
				if (pfImpact != null)
					Instantiate(pfImpact, (rbProjectile.position), Quaternion.identity, enemy.transform);
				Destroy();																				// Run destroy method after hitting target maybe I should put this in a method
			}
		}
		else 
		{
			Hitcalc player = hitInfo.transform.gameObject.GetComponent<Hitcalc>();
			if (player != null & hitInfo.transform.gameObject.CompareTag("Player"))
			{
				Debug.Log("Player taking damage");
				player.takeDamage(iDamage);
				if (pfImpact != null)
					Instantiate(pfImpact, (rbProjectile.position), Quaternion.identity, player.transform);
				Destroy();																				// Run destroy method after hitting target
			}
		}
	}

	void Destroy()
	{
		v3Speed = Vector2.zero;
		gameObject.SetActive(false);
	}

	void OnDisable()
	{
		CancelInvoke();
	}

	void OnEnable()
	{
		Invoke("Destroy", fDeathtimer);
	}
}
