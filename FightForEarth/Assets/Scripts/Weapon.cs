﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

	public Transform firePoint;				// the point in space from which is fired
	public PoolingScript myPool;			// Projectile pooling script
	public GameObject pfProjectile;			// Projectile prefab
	public GameObject pfProjectileEffect;	// Shooting effect
	public bool bPlayerOwned;				// is shot by player or not
	public float fireRate;                  // cooldown before next shot / shots per second
	private float lastShot;					// timestamp of moment when the last shot has been fired			

	void Start()
	{
		if (pfProjectileEffect != null)
		{
			pfProjectileEffect.SetActive(false);	// make animation invisible till bullet is actually shot
		}
		lastShot = Time.time;						// To keep track of firerate
		myPool = GetComponent<PoolingScript>();
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetButton("Fire1") & bPlayerOwned == true & (lastShot + fireRate < Time.time))
		{
			Shoot();
			lastShot = Time.time;
		}
	}

	public void Shoot()
	{
		// Shooting logic
		if (pfProjectile != null)
		{
			GameObject tProjectile = myPool.GetPooledObject();											// Using pooled objects might create issue if target is not pooled
			if (tProjectile != null)
			{
				tProjectile.transform.position = firePoint.position;									// Move pooled prefab to firing position.
				//tProjectile.GetComponent<Projectile>().shotByPlayer = bPlayerOwned;
				if (tProjectile.GetComponent<HitDetection>() != null)
				{
					if (string.Equals(gameObject.layer.ToString(), "enemy"))							// Look at weapons own tag to see which layer it needs to collide with.
						tProjectile.GetComponent<HitDetection>().Layer = LayerMask.GetMask("player");
					else
						tProjectile.GetComponent<HitDetection>().Layer = LayerMask.GetMask("Enemy");
				}
				tProjectile.SetActive(true);															// Set the projectile from the pool as active
			}
			if (pfProjectileEffect != null)
			{
				pfProjectileEffect.SetActive(true);	// make animation visible, animation will have to be made invisible with animation event
			}
		}
	}
}
