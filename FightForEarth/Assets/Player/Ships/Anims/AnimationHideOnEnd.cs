﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHideOnEnd : MonoBehaviour {

	void HideOnAnimationEnd()
	{
		gameObject.SetActive(false);
	}
}
