﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// This class is for things that can take damage
public class Hitcalc: MonoBehaviour {
	public float iHealth;
	public float iArmor;
	public float iShield;

	public void takeDamage(float damage)
	{
		applyDamage(ref damage, ref iArmor, ref iHealth, 0);                        // call damagecalc script
		if (iHealth == 0 || iHealth < 0)                                            // check when ded
		{
			Destroy(gameObject);                                        			// destroy Enemy when ded
		}
	}

	public void applyDamage(ref float Damage, ref float Armor, ref float Health, int Damagetype)
	{
		if (Armor > 0)
		{
			if (Damage > Armor)
			{
				Damage -= Armor;
				Armor = 0;
			}
			else
			{
				Armor -= Damage;
				Damage = 0;
			}
		}
		if (Health < Damage)
		{
			Health = 0;
		}
		else
		{
			Health -= Damage;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
