﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideInGameplay : MonoBehaviour {

	private SpriteRenderer myRenderer;
	// Use this for initialization

	void Start () {
		myRenderer = GetComponent<SpriteRenderer>();
		myRenderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
