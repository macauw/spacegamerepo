﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player: MonoBehaviour
{
	public float verticalSpeed;
	public float horizontalSpeed;
	public float MaxSpeed;
	public float Acceleration;
	public float Deceleration;
	float RightShipBorder;
	float LeftShipBorder;
	private bool lWingInstantiated;
	private bool rWingInstantiated;
	private Vector3 nextPosition;
	public Transform myRb;
	public Transform wingPoint;
	public GameObject pfLWing;
	public GameObject pfRWing;
	private GameObject myGameObject;
	SpriteRenderer mySprite;
	Vector3 cameraPositionToEdge;
	bool RightMovement;
	bool RightBlocked;
	bool LeftMovement;
	bool LeftBlocked;
	bool UpMovement;
	bool UpBlocked;
	bool DownMovement;
	bool DownBlocked;

	// Use this for initialization
	void Start()
	{
		myRb = GetComponent<Transform>();
		horizontalSpeed = 0;
		verticalSpeed = 0;
		cameraPositionToEdge = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.nearClipPlane));// transform camera coordinates to world coordinates
	}

	// Update is called once per frame
	void Update()
	{
		DetectInputs();
		// instantiating the left wing
		if (pfLWing != null & !lWingInstantiated)
		{
			Debug.Log("instantiating Lwing");
			Instantiate(pfLWing, wingPoint.position, wingPoint.rotation, transform);	// Instantiate wing
			lWingInstantiated = true;
			mySprite = pfLWing.GetComponent<SpriteRenderer>();
			LeftShipBorder = mySprite.sprite.bounds.extents.x * 2;
			Debug.Log("Right wing border is " + LeftShipBorder);
		}
		if (pfLWing == null) 
		{
			lWingInstantiated = false;
			mySprite = GetComponent<SpriteRenderer>();
			LeftShipBorder = mySprite.sprite.bounds.extents.x / 2;
		}

		// instantiating the right wing
		if (pfRWing != null & !rWingInstantiated)
		{
			Debug.Log("instantiating Rwing");
			Instantiate(pfRWing, wingPoint.position, wingPoint.rotation, transform);    // Instantiate wing
			rWingInstantiated = true;
			mySprite = pfRWing.GetComponent<SpriteRenderer>();
			RightShipBorder = mySprite.sprite.bounds.extents.x * 2;
			Debug.Log("Right wing border is " + RightShipBorder);
		}
		if (pfRWing == null)
		{
			rWingInstantiated = false;
			mySprite = GetComponent<SpriteRenderer>();
			LeftShipBorder = mySprite.sprite.bounds.extents.x / 2;
		}

		// updating positions to make the ship move only in the X plane

	}

	void FixedUpdate()
	{
		CalculateSpeed();
		Vector3 temp = new Vector3(horizontalSpeed, verticalSpeed, 0);
		nextPosition = temp.normalized;
		nextPosition.x = Mathf.Abs(horizontalSpeed) * Time.deltaTime * nextPosition.x;
		nextPosition.y = Mathf.Abs(verticalSpeed) * Time.deltaTime * nextPosition.y;
		nextPosition.z = 0;
		Vector3 shipPosition = (myRb.transform.position);
		Debug.Log("Ship Position is " + shipPosition);
		Debug.Log("Camera vector is " + cameraPositionToEdge);
		Debug.Log("Next position is " + nextPosition);
		Debug.Log("Shipwidth is " + RightShipBorder);
		if ((nextPosition.x + shipPosition.x + RightShipBorder) >= cameraPositionToEdge.x)
		{
			nextPosition.x = shipPosition.x + RightShipBorder - Mathf.Clamp((shipPosition.x + RightShipBorder + nextPosition.x), (-1 * cameraPositionToEdge.x), (cameraPositionToEdge.x));
			if (RightMovement)
			{
				RightBlocked = true;
			}
			Debug.Log("Too far to the right");
		}
		else
		{
		}
		if ((shipPosition.x + nextPosition.x - LeftShipBorder) <= -1 * cameraPositionToEdge.x)
		{
			nextPosition.x = shipPosition.x - LeftShipBorder + cameraPositionToEdge.x;
			Debug.Log("Too far to the left");
		}

		Debug.Log("Camera width is " + cameraPositionToEdge.x);
		myRb.transform.Translate(nextPosition);
	}

	void DetectInputs()
	{
		//	Movement
		if (Input.GetKey("d") || Input.GetKey(KeyCode.RightArrow))
		{
			RightMovement = true;
		}
		else
		{
			RightMovement = false;
		}
		if (Input.GetKey("a") || Input.GetKey(KeyCode.LeftArrow))
		{
			LeftMovement = true;
		}
		else
		{
			LeftMovement = false;
		}
		if (Input.GetKey("w") || Input.GetKey(KeyCode.UpArrow))
		{
			UpMovement = true;
		}
		else
		{
			UpMovement = false;
		}
		if (Input.GetKey("s") || Input.GetKey(KeyCode.DownArrow))
		{
			DownMovement = true;
		}
		else
		{
			DownMovement = false;
		}
	}

	void CalculateSpeed()
	{
		if (horizontalSpeed != MaxSpeed & RightMovement & !RightBlocked & !LeftMovement)
		{
			horizontalSpeed = horizontalSpeed + Acceleration * Time.deltaTime;
			if (horizontalSpeed > MaxSpeed)
			{
				horizontalSpeed = MaxSpeed;
			}
		}
		if (horizontalSpeed * -1 != MaxSpeed & LeftMovement & !LeftBlocked & !RightMovement)
		{
			horizontalSpeed = horizontalSpeed - Acceleration * Time.deltaTime;
			if (horizontalSpeed * -1 > MaxSpeed)
			{
				horizontalSpeed = MaxSpeed * -1;
			}
		}
		if (!(LeftMovement ^ RightMovement) & (horizontalSpeed != 0))
		{
			if (horizontalSpeed < 0)
			{
				horizontalSpeed = horizontalSpeed + Deceleration * Time.deltaTime;
				if (horizontalSpeed > 0)
				{
					horizontalSpeed = 0;
				}
			}
			if (horizontalSpeed > 0)
			{
				horizontalSpeed = horizontalSpeed - Deceleration * Time.deltaTime;
				if (horizontalSpeed < 0)
				{
					horizontalSpeed = 0;
				}
			}
		}
		if (verticalSpeed != MaxSpeed & UpMovement & !UpBlocked & !DownMovement)
		{
			verticalSpeed = verticalSpeed + Acceleration * Time.deltaTime;
			if (verticalSpeed > MaxSpeed)
			{
				verticalSpeed = MaxSpeed;
			}
		}
		if (verticalSpeed * -1 != MaxSpeed & DownMovement & !DownBlocked & !UpMovement)
		{
			verticalSpeed = verticalSpeed - Acceleration * Time.deltaTime;
			if (verticalSpeed * -1 > MaxSpeed)
			{
				verticalSpeed = MaxSpeed * -1;
			}
		}
		if (!(UpMovement ^ DownMovement) & (verticalSpeed != 0))
		{
			if (verticalSpeed < 0)
			{
				verticalSpeed = verticalSpeed + Deceleration * Time.deltaTime;
				if (verticalSpeed > 0)
				{
					verticalSpeed = 0;
				}
			}
			if (verticalSpeed > 0)
			{
				verticalSpeed = verticalSpeed - Deceleration * Time.deltaTime;
				if (verticalSpeed < 0)
				{
					verticalSpeed = 0;
				}
			}
		}
		if (UpBlocked)
		{
			if (verticalSpeed > 0)
			{
				verticalSpeed = 0;
			}
		}
		if (DownBlocked)
		{
			if (verticalSpeed < 0)
			{
				verticalSpeed = 0;
			}
		}
		if (RightBlocked)
		{
			if (horizontalSpeed > 0)
			{
				horizontalSpeed = 0;
			}
			Debug.Log("Right Blocked");
			if (RightBlocked & !RightMovement & LeftMovement)
			{
				RightBlocked = false;
			}
		}
		if (LeftBlocked)
		{
			if (horizontalSpeed < 0)
			{
				horizontalSpeed = 0;
			}
		}
	}
}