﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDetection : MonoBehaviour
{

	Rigidbody2D myRigidbody;
	Proj_Class myProjectile;
	int layerMask;                  // Layermask that the raycast triggers on
	Vector3 Velocity;               // Vector required for distance of raycast in 2D space
	Vector2 v2Direction;            // Direction of raycast in 2D space
	RaycastHit2D ray_Results;
	Hitcalc myhitcalc;

	// Use this for initialization
	void Start()
	{
		myRigidbody = GetComponent<Rigidbody2D>();
		myProjectile = GetComponent<Proj_Class>();
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if (myRigidbody != null || myProjectile != null)
		{
			v2Direction = myProjectile.direction;
			Velocity = myProjectile.nextposition;
			ray_Results = Physics2D.Raycast(myRigidbody.position, v2Direction, Velocity.magnitude, layerMask);
			if (ray_Results)                                                                // If raycast hits something with the right layer then
			{
				Vector3 tVector3 = new Vector3();
				tVector3 = Vector3.zero;                                            // Store distance in a temporary vector
				tVector3 = ray_Results.distance * v2Direction;
				myProjectile.nextposition = tVector3;                               // Move projectile to target collider will make it seem it hit the target
				tVector3 = myRigidbody.position;
				myProjectile.destination = tVector3 + myProjectile.nextposition;
				myProjectile.deactivate = true;
				HitReg(ray_Results);
			}
		}
	}
	void HitReg(RaycastHit2D hitInfo)
	{
		if (myhitcalc = hitInfo.transform.gameObject.GetComponent<Hitcalc>())
		{
			if (myhitcalc != null)
			{
				myhitcalc.takeDamage(myProjectile.Damage);
			}                                                                          
		}
	}

	public int Layer
	{
		set
		{
			layerMask = value;
		}
	}
}
