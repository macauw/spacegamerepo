﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proj_Class : MonoBehaviour {
	[SerializeField]
	float damage;											// amount of damage of the projectile
	[SerializeField]
	float Speed;                                            // Current velocity of projectile
	[SerializeField]
	float Accel;                                            // Acceleration of the projectile
	[SerializeField]
	float MaxSpeed;                                         // Maximum velocity of the projectile
	[SerializeField]
	float RawAngle;                                         // Angle that the projectile will start at
	float RawAngleDelta;
	float ConvertedAngle;                                   // Convert angle degrees into 2pi to be used with cosinus/sinus math
	[SerializeField]
	float AngularSpeed;                                     // Acceleration of angle in degrees/s
	float CurrentAngle;										// Current Angle of the projectile
	Rigidbody2D rbProjectile;								// Rigidbody of projectile 
	Vector2 Direction;										// 2D normalized direction vector, aims to direction of the projectile
	Vector3 NextPosition;                                   // Next position use this to manipulate the projectile
	Vector3 Destination;
	bool DeActivate;

	// Use this for initialization
	void Start () {
		DeActivate = false;
		rbProjectile = GetComponent<Rigidbody2D>();
		rbProjectile.transform.Rotate(0, 0, RawAngle);      // Have projectile match the rotation
		CurrentAngle = RawAngle;
	}

	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate()
	{
		if (DeActivate == false)
		{
			RawAngleDelta = AngularSpeed * Time.deltaTime;
			CurrentAngle += RawAngleDelta;
			ConvertedAngle = (2 * Mathf.PI) / 360 * (CurrentAngle + 270);       // Convert angle to something cosinus and sinus can understand
			Direction.x = Mathf.Cos(ConvertedAngle);
			Direction.y = Mathf.Sin(ConvertedAngle);                    // Direction vector is basicly normalized due to sinus, cosinus
			Speed = Speed + Accel * Time.deltaTime;                     // Calculate speed based on acceleration
			if (Speed > MaxSpeed)
			{
				Speed = MaxSpeed;
			}
			NextPosition.x = Direction.x * (Speed * Time.deltaTime);    // Calculate displacement by using the normalized vector from the angle calculation
			NextPosition.y = Direction.y * (Speed * Time.deltaTime);
			NextPosition.z = 0;
		}
		rbProjectile.transform.eulerAngles = new Vector3 (0, 0, CurrentAngle);
		rbProjectile.transform.Translate(NextPosition, Space.World);    // Actually move the projectiles rigidbody in relation to the world plane so rotated gameobject doesnt matter.
		if (DeActivate)
		{
			Deactivate();
			DeActivate = false;
		}
			
	}

	void Deactivate()
	{
		Speed = 0;
		CurrentAngle = RawAngle;
		gameObject.SetActive(false);
	}

	public float Velocity
	{
		set
		{
			if (value < MaxSpeed)
			{
				Speed = value;
			}
			else
			{
				Speed = MaxSpeed;
			}
			if (value < 0)
			{
				Speed = 0;
			}
		}
	}

	void OnBecameInvisible()
	{
		DeActivate = true;
	}

	public float Acceleration
	{
		set
		{
			Accel = value;
		}
	}

	public float MaximumSpeed
	{
		set
		{
			MaxSpeed = value;
		}
	}

	public float Angle
	{
		set
		{
			RawAngle = value;
		}
	}

	public float AngularVelocity
	{
		set
		{
			AngularSpeed = value;
		}
	}

	public float Damage
	{
		set
		{
			damage = value;
		}
		get
		{
			return damage;
		}
	}

	public bool deactivate
	{
		set
		{
			DeActivate = value;
		}
	}

	public Vector2 direction
	{
		set
		{
			Direction = value;
		}
		get
		{
			return Direction;
		}
	}

	public Vector3 nextposition
	{
		set
		{
			NextPosition = value;
		}
		get
		{
			return NextPosition;
		}
	}

	public Vector3 destination
	{
		set
		{
			Destination = value;
		}
		get
		{
			return Destination;
		}
	}
}
