﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCalc : MonoBehaviour {

	public void applyDamage(int Damage ,ref int Armor ,ref int Health, int Damagetype)
	{
		if (Armor > 0)
		{
			if (Armor < Damage)
			{
				Armor = 0;
			}
			else
			{
				Armor = Armor - Damage;
			}
		}
		else
		{
			if (Health < Damage)
			{
				Health = 0;
			}
			else
			{
				Health = Health - Damage;
			}
		}
		Damagetype ++;
	}
}
