﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public float fFireRate;
	private float dTime;
	private float myFireRate;
	public bool bSetDifficulity;
	public enum DIFFICULITY { EASY, NORMAL, HARD, IMPOSSIBLE };
	public DIFFICULITY myDifficulity;
	public Weapon wLeftWeapon;
	public Weapon wRightWeapon;

	void Start()
	{
		myFireRate = retFireRate(myDifficulity, fFireRate);
		bSetDifficulity = true;
		dTime = Time.time;
	}

	void Update()
	{
		if (bSetDifficulity == false)
		{

		}
		if (dTime < Time.time)
		{
			if (wLeftWeapon != null)
			{
				wLeftWeapon.Shoot();
			}
			if (wRightWeapon != null)
			{
				wRightWeapon.Shoot();
			}
			dTime = Time.time + myFireRate;
		}
	}

	void Fixedupdate()
	{
		
	}

	private float retFireRate(DIFFICULITY inDifficulity, float setFireRate)
	{
		float tempFireRate;
		if (inDifficulity == DIFFICULITY.EASY)
		{
			tempFireRate = setFireRate * 1.2f;
			return tempFireRate;
		}
		if (inDifficulity == DIFFICULITY.NORMAL)
		{
			tempFireRate = setFireRate * 1f;
			return tempFireRate;
		}
		if (inDifficulity == DIFFICULITY.HARD)
		{
			tempFireRate = setFireRate * 0.8f;
			return tempFireRate;
		}
		if (inDifficulity == DIFFICULITY.IMPOSSIBLE)
		{
			tempFireRate = setFireRate * 0.6f;
			return tempFireRate;
		}
		return setFireRate;
	}
}
